import type { ModuleConfig } from "/@/cool";
import "element-plus/theme-chalk/src/index.scss";
import "/@/modules/base/static/css/index.scss";

export default (): ModuleConfig => {
	return {
		views: [
			{
				meta: {
					label: "Payment"
				},
				path: "/payment/partner-list",
				component: () => import("./views/partner-list.vue")
			}
		]
	};
};
