// cl-upsert
import { setFocus } from "@cool-vue/crud";

const routeUpsert = {
	dialog: {
		width: "800px"
	},

	items: [
		{
			prop: "name",
			label: "路由名字",
			span: 12,
			required: true,
			component: {
				name: "el-input"
			}
		},
		{
			prop: "path",
			label: "路径",
			span: 12,
			required: true,
			component: {
				name: "el-input"
			}
		},
		{
			prop: "enabled",
			label: "状态",
			span: 12,
			required: true,
			// minWidth: 20,
			// value: 1,
			component: {
				name: "cl-switch"
			}
		},
		{
			prop: "sort",
			label: "排序",
			required: true,
			span: 12,
			component: {
				name: "el-input"
			}
			// minWidth: 20
		},
		{
			prop: "uri",
			label: "目标",
			required: true,
			component: {
				name: "el-input"
			}
		},
		{
			prop: "remark",
			label: "备注",
			component: {
				name: "el-input",
				props: {
					type: "textarea",
					rows: 4
				}
			}
		}
	],
	plugins: [setFocus("name")]
};

export function useRouteUpsert() {
	return { routeUpsert };
}
