import type { ModuleConfig } from "/@/cool";
import "element-plus/theme-chalk/src/index.scss";
import "/@/modules/base/static/css/index.scss";

export default (): ModuleConfig => {
	return {
		views: [
			{
				meta: {
					label: "Delivery"
				},
				path: "/delivery/partner-list",
				component: () => import("./views/partner-list.vue")
			}
		]
	};
};
