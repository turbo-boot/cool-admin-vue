import type { ModuleConfig } from "/@/cool";
import "element-plus/theme-chalk/src/index.scss";
import "/@/modules/base/static/css/index.scss";

export default (): ModuleConfig => {
	return {
		views: [
			{
				meta: {
					label: "CONFIG"
				},
				path: "/crawler/config",
				component: () => import("./views/config.vue")
			}
		]
	};
};
