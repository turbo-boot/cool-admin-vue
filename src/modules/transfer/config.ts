import type { ModuleConfig } from "/@/cool";
import "element-plus/theme-chalk/src/index.scss";
import "/@/modules/base/static/css/index.scss";

export default (): ModuleConfig => {
	return {
		views: [
			{
				meta: {
					label: "transfer"
				},
				path: "/transfer/index",
				component: () => import("./views/transfer-handler.vue")
			}
		]
	};
};
