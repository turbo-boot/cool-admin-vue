declare namespace Eps {
	interface BaseSysDepartmentEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface BaseSysLogEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface CrawlerSofrEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface CrawlerConfigEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface CrawlerGroupEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface CrawlerNationalDebtEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface CrawlerExchangeRateEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface CrawlerResultEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface BohCargoEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface BohWarehouseEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface BohStockEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface BohStockTransactionEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface BohPackageEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface BohAcceptanceOrderEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface BohReturnOrderEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface BohSalesOrderEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface BohTransferOrderEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface BohDisposeOrderEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface BohStockCheckOrderEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface BohStockCheckTemplateEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface BohStockCheckPlanEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface BaseSysMenuEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface BaseSysParamEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface BaseSysRoleEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface BaseSysUserEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface DemoGoodsEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface DictInfoEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface DictTypeEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface PluginInfoEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface RecycleDataEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface SpaceInfoEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface SpaceTypeEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TaskInfoEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface UserAddressEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface UserInfoEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TransferHandlerEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboDeliveryPartnerEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboDeliveryServiceEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboPaymentServiceEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboPrinterServiceEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboTemplateGroupEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}
	interface TurboPrinterTemplateEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}
	interface TurboPrinterOrderEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}
	interface TurboPrinterLogEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboDeliveryGatewayEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboPaymentOrderEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboPaymentPartnerEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboPrinterPartnerEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboPaymentGatewayEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboPrinterGatewayEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboSaasTenantEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboSaasAppEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboSaaSOpenIsvEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboSaasThirdAppEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboSaasServiceGroupEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboSaasServiceAuthEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboSaasOpenServiceEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboDeliveryAppAuthEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboPaymentAppAuthEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboPrinterAppAuthEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboPrinterTerminalEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboDeliveryOrderEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboDeliveryShopMappingEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboMdmUnitDefEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboMdmUnitEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboRouteEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboPredicateEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TurboFilterInfoEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface ChatMessageEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface ChatSessionEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface TestEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface DemoUserFollowEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}

	interface DemoUserInfoEntity {
		/**
		 * 任意键值
		 */
		[key: string]: any;
	}
	interface BaseComm {
		/**
		 * personUpdate
		 */
		"personUpdate"(data?: any): Promise<any>;
		/**
		 * uploadMode
		 */
		"uploadMode"(data?: any): Promise<any>;
		/**
		 * permmenu
		 */
		"permmenu"(data?: any): Promise<any>;
		/**
		 * person
		 */
		"person"(data?: any): Promise<any>;
		/**
		 * upload
		 */
		"upload"(data?: any): Promise<any>;
		/**
		 * logout
		 */
		"logout"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			personUpdate: string;
			uploadMode: string;
			permmenu: string;
			person: string;
			upload: string;
			logout: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			personUpdate: boolean;
			uploadMode: boolean;
			permmenu: boolean;
			person: boolean;
			upload: boolean;
			logout: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface BaseOpen {
		/**
		 * refreshToken
		 */
		"refreshToken"(data?: any): Promise<any>;
		/**
		 * captcha
		 */
		"captcha"(data?: any): Promise<any>;
		/**
		 * login
		 */
		"login"(data?: any): Promise<any>;
		/**
		 * html
		 */
		"html"(data?: any): Promise<any>;
		/**
		 * eps
		 */
		"eps"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			refreshToken: string;
			captcha: string;
			login: string;
			html: string;
			eps: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			refreshToken: boolean;
			captcha: boolean;
			login: boolean;
			html: boolean;
			eps: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface BaseSysDepartment {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * order
		 */
		"order"(data?: any): Promise<any>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<BaseSysDepartmentEntity[]>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: { delete: string; update: string; order: string; list: string; add: string };
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			order: boolean;
			list: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface BaseSysLog {
		/**
		 * setKeep
		 */
		"setKeep"(data?: any): Promise<any>;
		/**
		 * getKeep
		 */
		"getKeep"(data?: any): Promise<any>;
		/**
		 * clear
		 */
		"clear"(data?: any): Promise<any>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: BaseSysLogEntity[];
			[key: string]: any;
		}>;
		/**
		 * 权限标识
		 */
		permission: { setKeep: string; getKeep: string; clear: string; page: string };
		/**
		 * 权限状态
		 */
		_permission: { setKeep: boolean; getKeep: boolean; clear: boolean; page: boolean };
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface CrawlerSofr {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: CrawlerSofrEntity[];
			[key: string]: any;
		}>;
		/**
		 * 权限标识
		 */
		permission: { page: string };
		/**
		 * 权限状态
		 */
		_permission: { page: boolean };
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface CrawlerConfig {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<CrawlerConfigEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<CrawlerConfigEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: CrawlerConfigEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface CrawlerGroup {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<CrawlerGroupEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<CrawlerGroupEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: CrawlerGroupEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface CrawlerNationalDebt {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: CrawlerNationalDebtEntity[];
			[key: string]: any;
		}>;
		/**
		 * 权限标识
		 */
		permission: { page: string };
		/**
		 * 权限状态
		 */
		_permission: { page: boolean };
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface CrawlerExchangeRate {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: CrawlerExchangeRateEntity[];
			[key: string]: any;
		}>;
		/**
		 * 权限标识
		 */
		permission: { page: string };
		/**
		 * 权限状态
		 */
		_permission: { page: boolean };
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface CrawlerResult {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: CrawlerResultEntity[];
			[key: string]: any;
		}>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<CrawlerResultEntity[]>;
		/**
		 * 权限标识
		 */
		permission: { page: string; list: string };
		/**
		 * 权限状态
		 */
		_permission: { page: boolean; list: boolean };
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface BohCargo {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<BohCargoEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<BohCargoEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: BohCargoEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface BohWarehouse {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<BohWarehouseEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<BohWarehouseEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: BohWarehouseEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface BohStock {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<BohStockEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<BohStockEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: BohStockEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface BohStockTransaction {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<BohStockTransactionEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<BohStockTransactionEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: BohStockTransactionEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface BohPackage {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<BohPackageEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<BohPackageEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: BohPackageEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface BohAcceptanceOrder {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<BohAcceptanceOrderEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<BohAcceptanceOrderEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: BohAcceptanceOrderEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface BohReturnOrder {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<BohReturnOrderEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<BohReturnOrderEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: BohReturnOrderEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface BohSalesOrder {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<BohSalesOrderEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<BohSalesOrderEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: BohSalesOrderEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface BohTransferOrder {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<BohTransferOrderEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<BohTransferOrderEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: BohTransferOrderEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface BohDisposeOrder {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<BohDisposeOrderEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<BohDisposeOrderEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: BohDisposeOrderEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface BohStockCheckOrder {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<BohStockCheckOrderEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<BohStockCheckOrderEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: BohStockCheckOrderEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface BohStockCheckTemplate {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<BohStockCheckTemplateEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<BohStockCheckTemplateEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: BohStockCheckTemplateEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface BohStockCheckPlan {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<BohStockCheckPlanEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<BohStockCheckPlanEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: BohStockCheckPlanEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface BaseSysMenu {
		/**
		 * create
		 */
		"create"(data?: any): Promise<any>;
		/**
		 * export
		 */
		"export"(data?: any): Promise<any>;
		/**
		 * import
		 */
		"import"(data?: any): Promise<any>;
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * parse
		 */
		"parse"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<BaseSysMenuEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<BaseSysMenuEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: BaseSysMenuEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			create: string;
			export: string;
			import: string;
			delete: string;
			update: string;
			parse: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			create: boolean;
			export: boolean;
			import: boolean;
			delete: boolean;
			update: boolean;
			parse: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface BaseSysParam {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * html
		 */
		"html"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<BaseSysParamEntity>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: BaseSysParamEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			html: string;
			info: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			html: boolean;
			info: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface BaseSysRole {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<BaseSysRoleEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<BaseSysRoleEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: BaseSysRoleEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface BaseSysUser {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * move
		 */
		"move"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<BaseSysUserEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<BaseSysUserEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: BaseSysUserEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			move: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			move: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface DemoGoods {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<DemoGoodsEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<DemoGoodsEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: DemoGoodsEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: DemoGoodsEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface DemoUserFollow {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: DemoUserFollowEntity[];
			[key: string]: any;
		}>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<DemoUserFollowEntity[]>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<DemoUserFollowEntity>;
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface DemoUserInfo {
		/**
		 * t1
		 */
		"t1"(data?: any): Promise<any>;
		/**
		 * t2
		 */
		"t2"(data?: any): Promise<any>;
		/**
		 * t3
		 */
		"t3"(data?: any): Promise<any>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: DemoUserInfoEntity[];
			[key: string]: any;
		}>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<DemoUserInfoEntity[]>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<DemoUserInfoEntity>;
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			t1: string;
			t2: string;
			t3: string;
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			t1: boolean;
			t2: boolean;
			t3: boolean;
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface DictInfo {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * data
		 */
		"data"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<DictInfoEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<DictInfoEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: DictInfoEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			data: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			data: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface DictType {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<DictTypeEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<DictTypeEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: DictTypeEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface PluginInfo {
		/**
		 * install
		 */
		"install"(data?: any): Promise<any>;
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<PluginInfoEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<PluginInfoEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: PluginInfoEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			install: string;
			delete: string;
			update: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			install: boolean;
			delete: boolean;
			update: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface RecycleData {
		/**
		 * restore
		 */
		"restore"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<RecycleDataEntity>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: RecycleDataEntity[];
			[key: string]: any;
		}>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<RecycleDataEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<RecycleDataEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: RecycleDataEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: { restore: string; info: string; page: string; list: string; add: string };
		/**
		 * 权限状态
		 */
		_permission: {
			restore: boolean;
			info: boolean;
			page: boolean;
			list: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface SpaceInfo {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<SpaceInfoEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<SpaceInfoEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: SpaceInfoEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface SpaceType {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<SpaceTypeEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<SpaceTypeEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: SpaceTypeEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TaskInfo {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * start
		 */
		"start"(data?: any): Promise<any>;
		/**
		 * once
		 */
		"once"(data?: any): Promise<any>;
		/**
		 * stop
		 */
		"stop"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<TaskInfoEntity>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TaskInfoEntity[];
			[key: string]: any;
		}>;
		/**
		 * log
		 */
		"log"(data?: any): Promise<any>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			start: string;
			once: string;
			stop: string;
			info: string;
			page: string;
			log: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			start: boolean;
			once: boolean;
			stop: boolean;
			info: boolean;
			page: boolean;
			log: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface UserAddress {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<UserAddressEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<UserAddressEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: UserAddressEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface UserInfo {
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<UserInfoEntity>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<UserInfoEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: UserInfoEntity[];
			[key: string]: any;
		}>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<UserInfoEntity[]>;
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: UserInfoEntity[];
			[key: string]: any;
		}>;
		/**
		 * 权限标识
		 */
		permission: {
			delete: string;
			update: string;
			info: string;
			list: string;
			page: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			delete: boolean;
			update: boolean;
			info: boolean;
			list: boolean;
			page: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TransferHandler {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TransferHandlerEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TransferHandlerEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TransferHandlerEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboDeliveryPartner {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboDeliveryPartnerEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboDeliveryPartnerEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboDeliveryPartnerEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboDeliveryService {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboDeliveryServiceEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboDeliveryServiceEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboDeliveryServiceEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboPaymentService {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboPaymentServiceEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboPaymentServiceEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboPaymentServiceEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboDeliveryGateway {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboDeliveryGatewayEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboDeliveryGatewayEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboDeliveryGatewayEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboPaymentOrder {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboPaymentOrderEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboPaymentOrderEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboPaymentOrderEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}
	interface TurboPaymentPartner {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboPaymentPartnerEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboPaymentPartnerEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboPaymentPartnerEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboPrinterPartner {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboPrinterPartnerEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboPrinterPartnerEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboPrinterPartnerEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}
	interface TurboPaymentGateway {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboPaymentGatewayEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboPaymentGatewayEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboPaymentGatewayEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboPrinterGateway {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboPrinterGatewayEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboPrinterGatewayEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboPrinterGatewayEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboSaasTenant {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboSaasTenantEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboSaasTenantEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboSaasTenantEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboSaasApp {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboSaasAppEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboSaasAppEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboSaasAppEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboSaaSOpenIsv {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboSaaSOpenIsvEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboSaaSOpenIsvEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboSaaSOpenIsvEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboSaasServiceGroup {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboSaasServiceGroupEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboSaasServiceGroupEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboSaasServiceGroupEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboSaasServiceAuth {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboSaasServiceAuthEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboSaasServiceAuthEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboSaasServiceAuthEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboSaaSOpenService {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboSaasOpenServiceEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboSaasOpenServiceEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboSaasOpenServiceEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboSaasThirdApp {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboSaasThirdAppEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboSaasThirdAppEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboSaasThirdAppEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboDeliveryAppAuth {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboDeliveryAppAuthEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboDeliveryAppAuthEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboDeliveryAppAuthEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboPaymentAppAuth {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboPaymentAppAuthEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboPaymentAppAuthEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboPaymentAppAuthEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboPrinterAppAuth {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboPrinterAppAuthEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboPaymentAppAuthEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboPaymentAppAuthEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboPrinterService {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboPrinterServiceEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboPrinterServiceEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboPrinterServiceEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboTemplateGroup {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboTemplateGroupEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboTemplateGroupEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboTemplateGroupEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboPrinterTemplate {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboPrinterTemplateEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboPrinterTemplateEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboPrinterTemplateEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboPrinterOrder {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboPrinterOrderEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboPrinterOrderEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboPrinterOrderEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboPrinterLog {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboPrinterLogEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboPrinterLogEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboPrinterLogEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboPrinterTerminal {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboPrinterTerminalEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboPrinterTerminalEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboPrinterTerminalEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboDeliveryOrder {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboDeliveryOrderEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboDeliveryOrderEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboDeliveryOrderEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboDeliveryShopMapping {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboDeliveryShopMappingEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboDeliveryShopMappingEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboDeliveryShopMappingEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboRoute {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboRouteEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboRouteEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboRouteEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboMdmUnit {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboMdmUnitEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboMdmUnitEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboMdmUnitEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboMdmUnitDef {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboMdmUnitDefEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboMdmUnitDefEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboMdmUnitDefEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			unitDefList: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			unitDefList: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboPredicate {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboPredicateEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboPredicateEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboPredicateEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface TurboFilterInfo {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TurboFilterInfoEntity[];
			[key: string]: any;
		}>;

		/**
		 * list
		 */
		"list"(data?: any): Promise<TurboFilterInfoEntity[]>;

		/**
		 * info
		 */
		"info"(data?: any): Promise<TurboFilterInfoEntity>;

		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;

		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;

		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;

		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};

		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface ChatMessage {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: ChatMessageEntity[];
			[key: string]: any;
		}>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<ChatMessageEntity[]>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<ChatMessageEntity>;
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface ChatSession {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: ChatSessionEntity[];
			[key: string]: any;
		}>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<ChatSessionEntity[]>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<ChatSessionEntity>;
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			list: string;
			info: string;
			delete: string;
			update: string;
			add: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			list: boolean;
			info: boolean;
			delete: boolean;
			update: boolean;
			add: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	interface Test {
		/**
		 * page
		 */
		"page"(data?: any): Promise<{
			pagination: { size: number; page: number; total: number; [key: string]: any };
			list: TestEntity[];
			[key: string]: any;
		}>;
		/**
		 * update
		 */
		"update"(data?: any): Promise<any>;
		/**
		 * add
		 */
		"add"(data?: any): Promise<any>;
		/**
		 * info
		 */
		"info"(data?: any): Promise<TestEntity>;
		/**
		 * delete
		 */
		"delete"(data?: any): Promise<any>;
		/**
		 * list
		 */
		"list"(data?: any): Promise<TestEntity[]>;
		/**
		 * 权限标识
		 */
		permission: {
			page: string;
			update: string;
			add: string;
			info: string;
			delete: string;
			list: string;
		};
		/**
		 * 权限状态
		 */
		_permission: {
			page: boolean;
			update: boolean;
			add: boolean;
			info: boolean;
			delete: boolean;
			list: boolean;
		};
		/**
		 * 请求
		 */
		request: Service["request"];
	}

	type json = any;

	type Service = {
		request(options?: {
			url: string;
			method?: "POST" | "GET" | "PUT" | "DELETE" | "PATCH" | "HEAD" | "OPTIONS";
			data?: any;
			params?: any;
			headers?: {
				[key: string]: any;
			};
			timeout?: number;
			proxy?: boolean;
			[key: string]: any;
		}): Promise<any>;
		base: {
			comm: BaseComm;
			open: BaseOpen;
			sys: {
				department: BaseSysDepartment;
				log: BaseSysLog;
				menu: BaseSysMenu;
				param: BaseSysParam;
				role: BaseSysRole;
				user: BaseSysUser;
			};
		};
		demo: { goods: DemoGoods; user: { follow: DemoUserFollow; info: DemoUserInfo } };
		dict: { info: DictInfo; type: DictType };
		plugin: { info: PluginInfo };
		recycle: { data: RecycleData };
		space: { info: SpaceInfo; type: SpaceType };
		task: { info: TaskInfo };
		user: { address: UserAddress; info: UserInfo };
		chat: { message: ChatMessage; session: ChatSession };
		delivery: {
			partner: TurboDeliveryPartner;
			gateway: TurboDeliveryGateway;
			auth: TurboDeliveryAppAuth;
			order: TurboDeliveryOrder;
			service: TurboDeliveryService;
			shopMapping: TurboDeliveryShopMapping;
		};
		payment: {
			partner: TurboPaymentPartner;
			gateway: TurboPaymentGateway;
			auth: TurboPaymentAppAuth;
			order: TurboPaymentOrder;
			service: TurboPaymentService;
		};
		printer: {
			partner: TurboPrinterPartner;
			gateway: TurboPrinterGateway;
			auth: TurboPrinterAppAuth;
			terminal: TurboPrinterTerminal;
			service: TurboPrinterService;
			group: TurboTemplateGroup;
			template: TurboPrinterTemplate;
			order: TurboPrinterOrder;
			log: TurboPrinterLog;
		};
		gateway: { route: TurboRoute; predicate: TurboPredicate; filter: TurboFilterInfo };
		saas: {
			tenant: TurboSaasTenant;
			app: TurboSaasApp;
			open: {
				isv: TurboSaaSOpenIsv;
				thirdApp: TurboSaasThirdApp;
				serviceGroup: TurboSaasServiceGroup;
				serviceAuth: TurboSaasServiceAuth;
				service: TurboSaaSOpenService;
			};
		};
		mdm: {
			unit: TurboMdmUnit;
			unitDef: TurboMdmUnitDef;
		};
		transfer: { handler: TransferHandler };
		crawler: {
			config: CrawlerConfig;
			result: CrawlerResult;
			group: CrawlerGroup;
		};
		boh: {
			cargo: BohCargo;
			warehouse: BohWarehouse;
			stock: BohStock;
			stockTransaction: BohStockTransaction;
			package: BohPackage;
			acceptanceOrder: BohAcceptanceOrder;
			returnOrder: BohReturnOrder;
			salesOrder: BohSalesOrder;
			transferOrder: BohTransferOrder;
			disposeOrder: BohDisposeOrder;
			stockCheckOrder: BohStockCheckOrder;
			stockCheckTemplate: BohStockCheckTemplate;
			stockCheckPlan: BohStockCheckPlan;
		};
		test: Test;
	};
}
